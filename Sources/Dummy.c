#include "../Headers/Dummy.h"
#include "../Headers/Alumnos.h"
#include <stdlib.h>
#include <string.h>

#define ASCII_START 32
#define ASCII_END 126
#define MAX_MATERIAS 10001
#define MAX_ALUMNOS 1000

/**
 * fn: randomString
 * @param size
 * @return
 */
char* randomString(int size) {
    int i;
    char *res = malloc(size + 1);
    for(i = 0; i < size; i++) {
        res[i] = (char) (rand()%(ASCII_END-ASCII_START))+ASCII_START;
    }
    res[i] = '\0';
    return res;
}

Materias* dummyMaterias() {
    Materias* materias = crearListaMaterias();
    for (int i = 0; i < MAX_MATERIAS; ++i) {
        materias = agregarMateria(
                materias,
                i,
                randomString(rand() % 50),
                randomString(rand() % 50),
                rand() % 2 + 1
        );
    }
    return materias;
}

Alumnos* dummyAlumnos() {
    Alumnos* alumnos = crearLista();

    for (int i = 0; i < MAX_ALUMNOS; ++i) {
        Alumno* alumno = malloc(sizeof(Alumno));
        alumno->legajo = i + 1;
        strcpy(alumno->nombre, randomString(rand() % 20));
        strcpy(alumno->apellido, randomString(rand() % 20));
        alumno->dni = rand() % 99999999 + 1;
        alumno->anio = 1970 + rand() % 33 + 1;
        alumnos = agregarAlumno(alumnos, alumno);
    }
    return alumnos;
}

int obtenerUltimoIdAlumno(Alumnos* lista, int id) {
    if (lista == NULL){
        return id + 1;
    }else{
        return obtenerUltimoIdAlumno(lista->proximo, lista->alumno->legajo);
    }
}

int obtenerUltimoIdMateria(Materias* lista, int id) {
    if (lista == NULL){
        return id + 1;
    }else{
        return obtenerUltimoIdMateria(lista->siguiente, lista->materia->id);
    }
}