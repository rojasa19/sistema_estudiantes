#include <stdio.h>
#include <stdlib.h>
#include "../Headers/Materias.h"

/*
 * fn: crearListaMaterias
 * @description: Funcion que crea una lista de materias
 */
Materias* crearListaMaterias(){
    Materias* nuevo = NULL;
    return nuevo;
}

/*
 * fn: agregarMateria
 * @description: Funcion que agrega una materia al listado
 */
Materias* agregarMateria(Materias* lista, int id, char nombre[], char correlativa[], int cuatrimestre) {
    Materias* nuevo = malloc(sizeof(Materias));
    nuevo->materia = crearMateria(id, nombre, correlativa, cuatrimestre);
    nuevo->siguiente = NULL;
    nuevo->nota = NULL;
    lista = agregar(lista, nuevo);
    return lista;
}

/*
 * fn: agregarMateria
 * @description: Funcion que agrega una materia al listado con una materia creada
 */
Materias* agregarMateriaCreada(Materias* lista, Materia* materia) {
    Materias* nuevo = malloc(sizeof(Materias));
    nuevo->materia = materia;
    nuevo->siguiente = NULL;
    nuevo->nota = NULL;
    lista = agregar(lista, nuevo);
    return lista;
}

/*
 * fn: agregar
 * @description: Funcion que agrega una materia al listado recursivamente
 */
Materias* agregar(Materias* lista, Materias* nuevo) {
    if(lista == NULL) {
        return nuevo;
    }else {
        lista->siguiente = agregar(lista->siguiente, nuevo);
    }
    return lista;
}

/*
 * fn: buscarMateria
 * @description: Funcion que busca una materia por su numero
 */
Materia* buscarMateria(Materias* lista, int id){
    if (lista == NULL){
        return NULL;
    }else{
        if (id == lista->materia->id){
            return lista->materia;
        }else{
            return buscarMateria(lista->siguiente, id);
        }
    }
}

/*
 * fn: imprimirListaMaterias
 * @description: Funcion que imprime el listado de materias
 */
void imprimirListaMaterias(Materias* lista){
    if (lista != NULL){
        printf("Nro Materia: %d - %s", lista->materia->id, lista->materia->nombre);
        printf("\n");
        imprimirListaMaterias(lista->siguiente);
    }
}