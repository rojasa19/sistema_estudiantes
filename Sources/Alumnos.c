#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../Headers/Alumnos.h"

/**
 * fn: crearLista
 * @description: Funcion que crea una lista de alumnos
 * @return
 */
Alumnos* crearLista(){
    Alumnos* nuevo = NULL;
    return nuevo;
}

 /**
  * fn: agregarAlumno
  * @description: Funcion que agrega un alumno a la lista
  * @param lista
  * @param alumno
  * @return
  */
Alumnos* agregarAlumno(Alumnos* lista, Alumno* alumno) {
    Alumnos* nuevo = malloc(sizeof(Alumnos));
    nuevo->alumno = alumno;
    nuevo->proximo = NULL;

    if(lista == NULL) {
        lista = nuevo;
    }else {
        Alumnos* aux = lista;
        while(aux->proximo != NULL) {
            aux = aux->proximo;
        }
        aux->proximo = nuevo;
    }
    return lista;
}

 /**
  * fn: imprimirListaAlumnos
  * @description: Funcion que imprime el listado de usuarios
  * @param lista
  */
void imprimirListaAlumnos(Alumnos* lista){
    Alumnos* aux = lista;
    while(aux != NULL) {
        printf("Nro Legajo: %d - %s %s\n", aux->alumno->legajo, aux->alumno->nombre, aux->alumno->apellido);
        aux = aux->proximo;
    }
    printf("\n");
}

 /**
  * fn: buscarAlumnoPorNombre
  * @description: Funcion que busca un alumno por su nombre
  * @param lista
  * @param nombre
  */
void buscarAlumnoPorNombre(Alumnos* lista, char* nombre){
    Alumnos* aux = lista;
    if (aux == NULL){
        printf("No hay mas alumnos en la lista\n");
        printf("\n");
    }else{
        if(strcmp(aux->alumno->nombre, nombre) == 0){
            printf("Nro Legajo: %d - %s %s\n", aux->alumno->legajo, aux->alumno->nombre, aux->alumno->apellido);
        }
        buscarAlumnoPorNombre(aux->proximo, nombre);
    }
}

/**
  * fn: buscarAlumnoPorRango
  * @description: Funcion que busca un alumno por su rango de edad
  * @param lista
  * @param edadDesde
  * @param edadHasta
  */
void buscarAlumnoPorRango(Alumnos* lista, int edadDesde, int edadHasta){
    Alumnos* aux = lista;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int auxDesde = tm.tm_year + 1900 - edadHasta;
    int auxHasta = tm.tm_year + 1900 - edadDesde;

    if (aux == NULL){
        printf("No hay mas alumnos en la lista\n");
        printf("\n");
    }else{
        if(auxDesde <= aux->alumno->anio && aux->alumno->anio <= auxHasta){
            printf("Nro Legajo: %d - %s %s\n", aux->alumno->legajo, aux->alumno->nombre, aux->alumno->apellido);
        }
        buscarAlumnoPorRango(aux->proximo, edadDesde, edadHasta);
    }
}

 /**
  * fn: rendirMateria
  * * @description: Funcion que agrega una nota a un usuario
  * @param lista
  */
void rendirMateria(Alumnos* lista){
    printf("Elija el alumno que rindio (Ingrese el Nro. legajo)\n");
    int idAlumno, idMateria;
    Alumnos* aux = lista;

    //Imprimir listado de alumnos
    while(aux != NULL) {
        printf("Nro Legajo: %d - %s %s\n", aux->alumno->legajo, aux->alumno->nombre, aux->alumno->apellido);
        aux = aux->proximo;
    }
    // Pido y busca alumno por ID
    scanf("%d", &idAlumno);
    Alumno* alumno = buscarAlumnoPorId(lista, idAlumno);
    Materias* materias = alumno->materias;

    // Recorre materias del alumno
    printf("Seleccione la materia rendida (Id)\n");
    while(materias != NULL) {
        printf("id: %d - %s\n", materias->materia->id, materias->materia->nombre);
        materias = materias->siguiente;
    }

    // Pido y busca alumno por ID
    scanf("%d", &idMateria);
    Materias* materia = buscarMateriaPorId(alumno->materias, idMateria);

    // Solicito nota de la materia rendida
    printf("Ingrese la nota del examen\n");
    scanf("%d", &materia->nota);

    printf("materia %s rendida con %d\n", materia->materia->nombre, materia->nota);
    printf("\n");
}

/**
 * fn: rendirMateria
 * * @description: Funcion que agrega una nota a un usuario
 * @param lista
 */
void anotarseMateria(Alumnos* lista, Materias* materias){
    printf("Elija el alumno que desea anotar a una materia (Ingrese el Nro. legajo)\n");
    int idAlumno;
    int idMateria;
    Alumnos* aux = lista;

    //Imprimir listado de alumnos
    while(aux != NULL) {
        printf("Nro Legajo: %d - %s %s\n", aux->alumno->legajo, aux->alumno->nombre, aux->alumno->apellido);
        aux = aux->proximo;
    }
    // Pido y busca alumno por ID
    scanf("%d", &idAlumno);
    Alumno* alumno = buscarAlumnoPorId(lista, idAlumno);
    agregarMateriaAlumno(alumno, materias, 1);
    printf("\n");
}

/**
 * fn: buscarAlumnoPorId
 * @description:
 * @param lista
 * @param id
 * @return
 */
Alumno* buscarAlumnoPorId(Alumnos* lista, int id) {
    if (lista == NULL){
       return NULL;
    }else{
        if(lista->alumno->legajo == id){
            return lista->alumno;
        }
        return buscarAlumnoPorId(lista->proximo, id);
    }
}

/**
 * fn: buscarMateriaPorId
 * @description:
 * @param lista
 * @param id
 * @return
 */
Materias* buscarMateriaPorId(Materias* lista, int id) {
    if (lista == NULL){
        return NULL;
    }else{
        if(lista->materia->id == id){
            return lista;
        }
        return buscarAlumnoPorId(lista->siguiente, id);
    }
}