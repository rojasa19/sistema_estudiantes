#include <stdlib.h>
#include "string.h"
#include "../Headers/Materia.h"

/*
 * fn: crearMateria
 * @description: Funcion que crea una materia
 */
Materia* crearMateria(int id, char nombre[], char correlativa[], int cuatrimestre) {
    Materia* materia = malloc(sizeof(Materia));
    materia->id = id;
    strcpy(materia->nombre, nombre);
    strcpy(materia->correlativa, correlativa);
    materia->cuatrimestre = cuatrimestre;
    return materia;
};