#include <stdio.h>
#include <stdlib.h>
#include "../Headers/Alumno.h"

/*
 * fn: crearAlumno
 * @description: Funcion que crea un alumno
 */
Alumno* crearAlumno(int id) {
    printf("Alta de nuevo alumno \n");
    Alumno* alumno = malloc(sizeof(Alumno));
    alumno->legajo = id;

    printf("Ingrese nombre:");
    scanf("%s", alumno->nombre);

    printf("Ingrese apellido:");
    scanf("%s", alumno->apellido);

    printf("Ingrese dni:");
    scanf("%d", &alumno->dni);

    printf("Ingrese anio de nacimiento: (yyyy):");
    scanf("%d", &alumno->anio);
    return alumno;
};

/*
 * fn: agregarMateriaAlumno
 * @description: Funcion que agrega una materia a un alumno
 */
Alumno* agregarMateriaAlumno(Alumno* alumno, Materias* materias, int buscar) {
    if(buscar == 0) {
        return alumno;
    }
    int idMateria;
    printf("Elija materias para inscribir al alumno: \n");
    imprimirListaMaterias(materias);

    printf("Elija por Nro Materia:");
    scanf("%d", &idMateria);

    //Funcion que busca una materia por el id
    Materia* materiaEncontrada = buscarMateria(materias, idMateria);
    alumno->materias = agregarMateriaCreada(alumno->materias, materiaEncontrada);
    printf("El alumno %s se inscribio a la materia %s\n",alumno->nombre , materiaEncontrada->nombre);

    printf("¿Desea agregar otra materia? 1) Si, 0) No \n");
    scanf("%d", &buscar);
    agregarMateriaAlumno(alumno, materias, buscar);
};