# sistema_estudiantes

#### Fecha de entrega: 2021-06-19

## Alumons

- Mauro Ludueña, legajo: 20388
- Adrian Rojas, xxxxxx

## Consigna del TP final:

**Desarrollar un sistema de estudiantes en C. Debe permitir:**

Decidir si se utiliza ordenamiento de los datos. El diseño del sistema y los modelos es libre. Utilizar la creatividad para que el manejo del sistema sea lo más práctico posible. Si los requerimientos planteados son cumplidos, el ejercicio está aprobado. 
Puede pasar que en situaciones en la que la cantidad de datos sea muy grande, el sistema sea inmanejable. En ese caso, detallar las limitaciones de la solución propuesta (si las limitaciones son reconocidas no se considera desaprobado el punto).
El trabajo se puede hacer de a dos personas o individualmente. En el caso de los estudiantes que rinden libre, en el caso de estar haciendo la cursada, es decir, si presentaron los TPs anteriores, pueden hacerlo en equipo de a dos si lo desean. En el caso en que se hayan anotado libre pero no han hecho la cursada ni los tps, tendrán que hacerlo individualmente.

## configuración

La configuración de la cantidad de usuarios y materias dummys se setea en el archivo Sources/Dummys.c.
- MAX_ALUMNOS.
- MAX_MATERIAS.


**Puntos obligatorios**

- [X] Dar de alta estudiantes
- [X] Listar estudiantes
- [X] Buscar estudiantes por nombre
- [X] Buscar estudiantes por rango de edad
- [X] Dar de alta materias
- [X] Listar materias
- [X] Anotarse en una materia
- [X] Rendir una materia
- [X] El sistema debe poder soportar un gran listado de estudiantes y materias. 
- [X] Utilizar estructuras de datos para almacenar los listados. 
- [X] Grabar un video de máximo 10 minutos (puede ser menos). En el caso de grupo de a dos, intenten participar los dos, pueden enviar dos videos de más o menos la misma duración (5 minutos máximo aproximadamente).

**Ideas no-obligatorias de implementar pero que suman puntos:**

- [ ] Utilizar paginado, poder elegir el estudiante/materia de un listado reducido.
- [X] Generar estudiantes de prueba y materias aleatorias de forma masiva.
- [ ] Estadísticas de los estudiantes y materias. 
- [ ] Árboles de correlatividad de materias. 
- [ ] Qué pasa si una materia anterior está desaprobada? Puede anotarse? Cálculo de promedios. 
- [X] Archivo de configuración general donde se especifican las variables del sistema.
- [x] En el repositorio hacer un README con los integrantes, las consignas implementadas y los puntos extras que hayan desarrollado.
