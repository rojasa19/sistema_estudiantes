#include <stdio.h>
#include "Headers/Alumno.h"
#include "Headers/Alumnos.h"
#include "Headers/Materias.h"
#include "Headers/Dummy.h"

int main() {
    // Proceso crear lista de materias
    Materias* materias = dummyMaterias();

    // Proceso de crear lista de alumnos
    Alumnos* alumnos = dummyAlumnos();

    printf("---------------------------------\n-- Bienvenido a UNTREF Systems --\n---------------------------------\n\n");
    int opcion;

    do {
        printf("¿En que podemos ayudarte hoy? (Ingrese el numero de opcion)");
        printf("\n   1) Dar de alta estudiante.");
        printf("\n   2) Listar estudiantes.");
        printf("\n   3) Buscar estudiante por nombre.");
        printf("\n   4) Buscar estudiante por rango de edad.");
        printf("\n   5) Rendir una materia.");
        printf("\n   6) Dar de alta una materia.");
        printf("\n   7) Listar materias.");
        printf("\n   8) Anotarse en una materia.");
        printf("\n   0) Salir.\n");
        scanf("%d", &opcion);

        switch(opcion){
            case 1:
                printf("Alta de nuevo estudiante");
                // Proceso de crear un alumnos
                Alumno* alumno1;
                if(materias == NULL) {
                    alumno1 = crearAlumno(1);
                } else {
                    alumno1 = crearAlumno(obtenerUltimoIdAlumno(alumnos, alumnos->alumno->legajo));
                }
                // Proceso de agregar el alumno a la lista de alumnos
                alumnos = agregarAlumno(alumnos, alumno1);
                break;
            case 2:
                printf("Lista de estudiantes\n");
                imprimirListaAlumnos(alumnos);
                break;
            case 3:
                printf("Buscar estudiante por nombre\n");
                printf("Ingrese el nombre del alumno:\n");
                char nombre[20];
                scanf("%s", nombre);
                buscarAlumnoPorNombre(alumnos, nombre);
                break;
            case 4:
                printf("Ingrese edad desde (procure que sea la menor)\n");
                int edadDesde;
                scanf("%d", &edadDesde);

                printf("Ingrese edad hasta (procure que sea la mayor)\n");
                int edadHasta;
                scanf("%d", &edadHasta);
                buscarAlumnoPorRango(alumnos, edadDesde, edadHasta);
                break;
            case 5:
                printf("Rendir materia de un alumno\n");
                rendirMateria(alumnos);
                break;
            case 6:
                printf("Alta de nueva materia\n");
                char nombreMateria[55], correlativaMateria[55];
                int cuatrimestre;

                printf("Ingrese nombre materia\n");
                scanf("%s", nombreMateria);
                printf("Ingrese nombre correlativa\n");
                scanf("%s", correlativaMateria);
                printf("Ingrese cuatrimestre\n");
                scanf("%d", &cuatrimestre);
                Materia * materia;
                if(materias == NULL) {
                    materia = crearMateria(1, nombreMateria, correlativaMateria, cuatrimestre);
                } else {
                    materia = crearMateria(obtenerUltimoIdMateria(materias, materias->materia->id), nombreMateria, correlativaMateria, cuatrimestre);
                }
                materias = agregarMateriaCreada(materias, materia);
                break;
            case 7:
                printf("Lista de materias\n");
                imprimirListaMaterias(materias);
                break;
            case 8:
                printf("Anotarse a una materia\n");
                anotarseMateria(alumnos, materias);
                break;
        }
    } while ( opcion != 0 );
    return 0;
}