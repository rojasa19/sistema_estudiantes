#ifndef TP4_DUMMY_H
#define TP4_DUMMY_H
#include "Materias.h"
#include "Alumnos.h"

char* randomString(int size);
Materias* dummyMaterias();
Alumnos* dummyAlumnos();
int obtenerUltimoIdAlumno(Alumnos* lista, int id);
int obtenerUltimoIdMateria(Materias* lista, int id);
#endif