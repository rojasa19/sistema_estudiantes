#ifndef TP4_MATERIA_H
#define TP4_MATERIA_H
typedef struct Materia {
    int id;
    char nombre[55];
    char correlativa[55];
    int cuatrimestre;
} Materia;

Materia* crearMateria(int id, char nombre[], char correlativa[], int cuatrimestre);

#endif //TP4_MATERIA_H
