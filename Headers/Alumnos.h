#include "Alumno.h"

#ifndef TP4_ALUMNOS_H
#define TP4_ALUMNOS_H
typedef struct Alumnos {
    Alumno* alumno;
    struct Alumnos* proximo;
} Alumnos;

Alumnos* crearLista();
Alumnos* agregarAlumno(Alumnos* lista, Alumno* alumno);
void imprimirListaAlumnos(Alumnos* lista);
void buscarAlumnoPorNombre(Alumnos* lista, char* nombre);
void buscarAlumnoPorRango(Alumnos* lista, int edadDesde, int edadHasta);
void rendirMateria(Alumnos* lista);
Alumno* buscarAlumnoPorId(Alumnos* lista, int id);
Materias* buscarMateriaPorId(Materias* lista, int id);
void anotarseMateria(Alumnos* lista, Materias* materias);

#endif //TP4_ALUMNOS_H
