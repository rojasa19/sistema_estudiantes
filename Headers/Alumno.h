#include "Materias.h"
#ifndef TP4_ALUMNO_H
#define TP4_ALUMNO_H

typedef struct Alumno {
    char nombre[20];
    char apellido[20];
    int dni;
    int anio;
    int legajo;
    Materias * materias;
} Alumno;

Alumno* crearAlumno(int id);
Alumno* agregarMateriaAlumno(Alumno* alumno, Materias* nueva, int buscar);

#endif //TP4_ALUMNO_H
