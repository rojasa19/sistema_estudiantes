#include "Materia.h"
#ifndef TP4_MATERIAS_H
#define TP4_MATERIAS_H
typedef struct Materias {
    Materia* materia;
    int nota;
    struct Materias* siguiente;
} Materias;

Materias* crearListaMaterias();
Materias* agregarMateria(Materias* lista, int id, char nombre[], char correlativa[], int cuatrimestre);
Materias* agregarMateriaCreada(Materias* lista, Materia* materia);
Materias* agregar(Materias* lista, Materias* nuevo);
Materia* buscarMateria(Materias* lista, int id);
void imprimirListaMaterias(Materias* lista);

#endif //TP4_MATERIAS_H
